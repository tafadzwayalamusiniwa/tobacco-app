package com.example.tafadzwa.tobacco;

public class Temp {
    private float temperature;
    private float humidity;

    public float getTemperature(){
        return temperature;
    }

    public float getHumidity(){
        return humidity;
    }
}
