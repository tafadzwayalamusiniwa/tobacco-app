package com.example.tafadzwa.tobacco;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Login extends AppCompatActivity {
    private Button button6;
    private Button button7;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        button6 = findViewById(R.id.Login);

        button6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openMainActivity();
            }

        });

        button7 = findViewById(R.id.Signup);

        button7.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                opensign_up();
            }

        });
    }

    public void openMainActivity()
    {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void opensign_up()
    {
        Intent intent = new Intent(this, sign_up.class);
        startActivity(intent);
    }
}
