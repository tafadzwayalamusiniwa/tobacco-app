package com.example.tafadzwa.tobacco;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {
    private Button button;
    private Button button1;
    private Button button2;
    private Button button3;
    private TextView textView;
    private TextView textView1;
    private TextView textView2;
    private TextView textView3;
    private Button button4;
    private ImageView image;
    private ImageView image1;
    private ImageView image2;
    private ImageView image3;






    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.contact);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                opencontact();
            }

        });



        button1= findViewById(R.id.graphs);
        button1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                opengraphs();
            }

        });

        button2= findViewById(R.id.team);
        button2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openteam();
            }

        });

        button3= findViewById(R.id.admin);
        button3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openadmin();
            }

        });


        textView= findViewById(R.id.news);
        textView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                opennews();
            }

        });
        textView1= findViewById(R.id.rateus);
        textView1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openRateus();
            }

        });

        textView2= findViewById(R.id.website);
        textView2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openwebsite();
            }

        });

        textView3= findViewById(R.id.apps);
        textView3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openapps();
            }

        });
        button4= findViewById(R.id.about);
        button4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openabout();
            }

        });

        image= findViewById(R.id.tnews);
        image.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                opennews();
            }

        });

        image1= findViewById(R.id.trateus);
        image1.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openRateus();
            }

        });

        image2= findViewById(R.id.twebsite);
        image2.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openwebsite();
            }

        });

        image3= findViewById(R.id.tapps);
        image3.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                openapps();
            }

        });


    }
    public void opencontact()
    {
        Intent intent = new Intent(this, contact.class);
        startActivity(intent);
    }

    public void opengraphs()
    {
        Intent intent = new Intent(this, graphs.class);
        startActivity(intent);
    }
    public void openteam()
    {
        Intent intent = new Intent(this, team.class);
        startActivity(intent);
    }

    public void openadmin()
    {
        Intent intent = new Intent(this, admin.class);
        startActivity(intent);
    }

    public void opennews()
    {
        Intent intent = new Intent(this, news.class);
        startActivity(intent);
    }
    public void openRateus()
    {
        Intent intent = new Intent(this, Rateus.class);
        startActivity(intent);
    }
    public void openwebsite()
    {
        Intent intent = new Intent(this, website.class);
        startActivity(intent);
    }

    public void openapps()
    {
        Intent intent = new Intent(this, apps.class);
        startActivity(intent);
    }
    public void openabout()
    {
        Intent intent = new Intent(this, about.class);
        startActivity(intent);
    }
}
