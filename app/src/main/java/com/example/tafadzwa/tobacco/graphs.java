package com.example.tafadzwa.tobacco;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.client.channel.ChannelEventListener;
import com.pusher.client.channel.SubscriptionEventListener;


import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.utils.ColorTemplate;


import com.google.gson.Gson;
import com.pusher.client.connection.ConnectionEventListener;
import com.pusher.client.connection.ConnectionStateChange;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static com.github.mikephil.charting.utils.ColorTemplate.VORDIPLOM_COLORS;

public class graphs extends AppCompatActivity {

    private LineChart mChart;

    private Pusher pusher;

    private static final String PUSHER_APP_KEY = "fd95c54f67e413a860dd";
    //private static final String PUSHER_APP_CLUSTER = "ap2";
    private static final String CHANNEL_NAME = "my-channel";
    private static final String EVENT_NAME = "my-event";

    private static final float HIGHEST_TEMP = 100f;
    private static final float LIMIT_MAX_TEMP = 70f;
    // public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graphs);


        mChart = (LineChart) findViewById(R.id.chart);

        setupChart();
        setupAxes();
        setupData();
        setLegend();

        /*PusherOptions options = new PusherOptions();
        options.setEncrypted(false);
        options.setCluster(PUSHER_APP_CLUSTER);*/
        pusher = new Pusher(PUSHER_APP_KEY);


        SubscriptionEventListener eventListener = new SubscriptionEventListener() {
            @Override
            public void onEvent(String channel, final String event, final String data) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Received event with data: " + data);
                        //Log.v(TAG, data);
                        Gson gson = new Gson();
                        Temp temp = gson.fromJson(data, Temp.class);
                        addEntry(temp);
                    }
                });
            }
        };

        //};



        pusher.connect(new ConnectionEventListener() {
            @Override
            public void onConnectionStateChange(ConnectionStateChange change) {
                System.out.println("State changed to " + change.getCurrentState() +
                        " from " + change.getPreviousState());
            }

            @Override
            public void onError(String message, String code, Exception e) {
                System.out.println("There was a problem connecting!");
            }
        });



        Channel channel = pusher.subscribe(CHANNEL_NAME);

        channel.bind(EVENT_NAME, eventListener);



    }


    private void setupChart() {
        // disable description text
        mChart.getDescription().setEnabled(false);
        // enable touch gestures
        mChart.setTouchEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);
        // enable scaling
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        // set an alternative background color
        mChart.setBackgroundColor(Color.DKGRAY);
    }

    private void setupAxes() {
        XAxis xl = mChart.getXAxis();
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);
        xl.setCenterAxisLabels(true);
        xl.setGranularity(1f); // one hour
        xl.setValueFormatter(new IAxisValueFormatter() {



            @Override
            public String getFormattedValue(float value, AxisBase axis) {

                SimpleDateFormat mFormat = new SimpleDateFormat("dd MMM HH:mm");
                Date now = new Date();
                String strDate = mFormat.format(now);
                long millis = TimeUnit.HOURS.toMillis((long) value);
                //return mFormat.format(new Date(millis));
                return strDate;
            }
        });

      /*  Date time = new java.util.Date(altimeStamp.get((int) value));
        SimpleDateFormat simpleDateFormatArrivals = new SimpleDateFormat("HH:mm:ss");

        TimeZone destinationTimezone = TimeZone.getTimeZone(
                Utils.getAppPrefString(mActivity, AppConstants.DEFAULT_TIMEZONE));
        simpleDateFormatArrivals.setTimeZone(destinationTimezone);
        return simpleDateFormatArrivals.format(time);

    }
}); */


        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaximum(HIGHEST_TEMP);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setTextColor(Color.WHITE);
        rightAxis.setAxisMaximum(100f);
        rightAxis.setAxisMinimum(0f);
        rightAxis.setDrawGridLines(true);
        rightAxis.setEnabled(true);

        // Add a limit line
        LimitLine ll = new LimitLine(LIMIT_MAX_TEMP, "Upper Limit");
        ll.setLineWidth(2f);
        ll.setLabelPosition(LimitLine.LimitLabelPosition.RIGHT_TOP);
        ll.setTextSize(10f);
        ll.setTextColor(Color.WHITE);
        // reset all limit lines to avoid overlapping lines
        leftAxis.removeAllLimitLines();
        leftAxis.addLimitLine(ll);
        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);


    }

    private void setupData() {
        LineData data = new LineData();
        //LineData data2 = new LineData();
        data.setValueTextColor(Color.WHITE);
        //data2.setValueTextColor(Color.WHITE);

        // add empty data
        mChart.setData(data);
        //mChart.setData(data2);
    }

    private void setLegend() {
        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.CIRCLE);
        l.setTextColor(Color.WHITE);
    }

    private LineDataSet createSet() {
        LineDataSet set = new LineDataSet(null, "Temperature Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColors(VORDIPLOM_COLORS[0]);
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(3f); //og 2f
        set.setCircleRadius(5f); //og 4f
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(15f); //og 10f
        // To show values of each point
        set.setDrawValues(true);

        return set;
    }

    private LineDataSet createSet2() {
        LineDataSet set2 = new LineDataSet(null, "Humidity Data");
        set2.setAxisDependency(YAxis.AxisDependency.LEFT);
        set2.setColors(VORDIPLOM_COLORS[2]);
        set2.setCircleColor(Color.YELLOW);
        set2.setLineWidth(3f); //og 2f
        set2.setCircleRadius(5f); //og 4f
        set2.setValueTextColor(Color.YELLOW);
        set2.setValueTextSize(15f); //og 10f
        // To show values of each point
        set2.setDrawValues(true);

        return set2;
    }

    private void addEntry(Temp temp) {
        LineData data = mChart.getData();
        LineData data2 = mChart.getData();
        System.out.println(temp.getHumidity());

        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            //ILineDataSet set2 = data.getDataSetByIndex(1);

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }


            data.addEntry(new Entry(set.getEntryCount(), temp.getTemperature()), 0);
            //data.addEntry(new Entry(set2.getEntryCount(), temp.getHumidity()), 1);


            // let the chart know it's data has changed
            data.notifyDataChanged();
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(15);

            // move to the latest entry
            mChart.moveViewToX(data.getEntryCount());
        }

        if (data2 != null) {

            ILineDataSet set2 = data2.getDataSetByIndex(1);



            if (set2 == null) {
                set2 = createSet2();
                data2.addDataSet(set2);
            }
            //data.addEntry(new Entry(set.getEntryCount(), temp.getTemperature()), 0);
            data2.addEntry(new Entry(set2.getEntryCount(), temp.getHumidity()), 1);


            // let the chart know it's data has changed
            data2.notifyDataChanged();
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(15);

            // move to the latest entry
            mChart.moveViewToX(data2.getEntryCount());
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        pusher.disconnect();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.save :
                mChart.saveToGallery("Chart",50);
                return true;
            case R.id.action_settings :
                return true;
            default: return super.onOptionsItemSelected(item);
        }
    }


}
