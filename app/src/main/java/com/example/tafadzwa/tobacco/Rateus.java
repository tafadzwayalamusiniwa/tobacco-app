package com.example.tafadzwa.tobacco;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

public class Rateus extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rateus);

        WebView mWebView = (WebView) findViewById(R.id.webview_rateus);
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl("https://startupbiz.co.zw/starting-tobacco-farming-business-plan-zimbabwe/");
    }
}
